<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('authenticate', 'Authentication@authenticate');

Route::middleware('jwt.auth')->get('/me', function (Request $request) {
    return JWTAuth::parseToken()->toUser();
});

Route::middleware([])->group(function () {
    Route::resource('users', 'Users\Users', [ 'except' => ['create', 'edit'] ]);
    Route::resource('profiles', 'Profiles\Profiles', [ 'except' => ['create', 'edit'] ]);

    #Groups
    Route::resource('groups', 'Groups\Groups', [ 'except' => ['create', 'edit'] ]);
    Route::resource('groups.installers', 'Groups\GroupsInstallers', [ 'except' => ['create', 'edit'] ]);
    Route::resource('groups.installations', 'Groups\GroupsInstallations', [ 'except' => ['create', 'edit'] ]);
    Route::resource('groups.installations.units', 'Groups\GroupsInstallationsUnits', [ 'except' => ['create', 'edit'] ]);

    #Installers
    Route::resource('installers', 'Installers\Installers', [ 'except' => ['create', 'edit'] ]);
    Route::resource('installers.installations', 'Installers\InstallersInstallations', [ 'except' => ['create', 'edit'] ]);
    Route::resource('installers.installations.units', 'Installers\InstallersInstallationsUnits', [ 'except' => ['create', 'edit'] ]);

    Route::resource('installations', 'Installations\Installations', [ 'except' => ['create', 'edit'] ]);
    Route::resource('installations.units', 'Installations\InstallationsUnits', [ 'except' => ['create', 'edit'] ]);

    Route::resource('units', 'Units\Units', [ 'except' => ['create', 'edit'] ]);
});

