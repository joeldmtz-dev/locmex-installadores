<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Installation extends Model
{
    use SoftDeletes;

    protected $fillable = ['address', 'installation_date', 'installation_time', 'group_id', 'installer_id'];

    protected $dates = ['deleted_at'];

    public function group() {
        return $this->belongsTo('App\Group');
    }

    public function installer() {
        return $this->belongsTo('App\Installer');
    }

    public function units() {
        return $this->hasMany('App\Unit');
    }


    public static function filter($dataSet, $params = []) {
        $installations = $dataSet
            ->orderBy('installation_date', 'asc')
            ->orderBy('installation_time', 'asc');

        if (isset($params['year'])) {
            $installations = $installations
                ->whereYear('installation_date', $params['year']);
        }

        if (isset($params['month'])) {
            $installations = $installations
                ->whereMonth('installation_date', $params['month']);
        }

        if (isset($params['after'])) {
            $installations = $installations
                ->whereDate('installation_date', '>=', $params['after']);
        }

        if (isset($params['before'])) {
            $installations = $installations
                ->whereDate('installation_date', '<=', $params['before']);
        }

        $installations = $installations->get();

        if (isset($params['byDate']) && $params['byDate']) {
            $collection = collect($installations->toArray());
            $installations = [$collection->groupBy('installation_date')->toArray()];

            if (isset($params['array']) && $params['array']) {
                $installations_array = [];

                foreach ($installations[0] as $key => $value) {
                    $installations_array[] = [
                        'date' => $key,
                        'events' => $value
                    ];
                }

                $installations = $installations_array;
            }
        }

        return $installations;
    }
}
