<?php
/**
 * Created by PhpStorm.
 * User: joeldmtz
 * Date: 7/18/17
 * Time: 5:30 PM
 */

namespace App\Library;

use App\Exceptions\InvalidRequestData;
use Validator;
use Illuminate\Http\Request;

class JSONValidator
{

    public static function validate(Request $request, $rules) {

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            throw new InvalidRequestData($validator->messages());
        }

        return true;
    }

}