<?php
/**
 * Created by PhpStorm.
 * User: joeldmtz
 * Date: 7/18/17
 * Time: 5:59 PM
 */

namespace App\Exceptions;

use Exception;
use Throwable;

class JsonHttpException extends Exception
{

    public function __construct($status = 500, $errors = [], $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(json_encode([
            'status' => $status,
            'error' => $errors
        ]), $status, $previous);
    }
}