<?php
/**
 * Created by PhpStorm.
 * User: joeldmtz
 * Date: 7/18/17
 * Time: 6:04 PM
 */

namespace App\Exceptions;

class InvalidRequestData extends JsonHttpException
{
    public function __construct($errors = [])
    {
        parent::__construct(400, $errors);
    }


}