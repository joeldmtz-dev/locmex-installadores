<?php
/**
 * Created by PhpStorm.
 * User: joeldmtz
 * Date: 7/19/17
 * Time: 9:05 AM
 */

namespace App\Exceptions;


use Throwable;

class ResourceNotFound extends JsonHttpException
{
    public function __construct($errors = [])
    {
        parent::__construct(404, $errors);
    }
}