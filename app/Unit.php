<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;

    protected $fillable = ['imei', 'installation_id'];

    protected $dates = ['deleted_at'];

    public function installation() {
        return $this->belongsTo('App\Installation');
    }
}
