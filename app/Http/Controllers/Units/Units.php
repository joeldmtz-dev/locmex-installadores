<?php

namespace App\Http\Controllers\Units;

use App\Http\Controllers\Controller;
use App\Library\JSONValidator;
use App\Unit;
use Illuminate\Http\Request;

class Units extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $params = $request->input();
        if (isset($params['onlyFree']) && $params['onlyFree']) {
            $units = Unit::whereNull('installation_id')->get();
        } else {
            $units = Unit::all();
        }

        return $units;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        JSONValidator::validate($request, [
           'imei' => 'required'
        ]);

        $unit = new Unit;
        $unit->fill($request->all());
        $unit->save();

        return $unit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = Unit::with('installation')->findOrFail($id);
        return $unit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = Unit::findOrFail($id);

        JSONValidator::validate($request, [
           'imei' => 'required'
        ]);

        $unit->fill($request->all());
        $unit->save();

        return $unit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::findOrFail($id);
        $unit->delete();
        return $unit;
    }
}
