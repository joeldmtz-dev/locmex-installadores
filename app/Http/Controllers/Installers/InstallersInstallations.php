<?php

namespace App\Http\Controllers\Installers;

use App\Http\Controllers\Controller;
use App\Installation;
use App\Installer;
use App\Library\JSONValidator;
use Illuminate\Http\Request;

class InstallersInstallations extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $installer_id)
    {
        $params = $request->input();
        $installer = Installer::findOrFail($installer_id);
        $installations = $installer->installations()->with('group', 'installer', 'units');
        return Installation::filter($installations, $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $installer_id)
    {
        $installer = Installer::findOrFail($installer_id);

        JSONValidator::validate($request, [
            'address' => 'required',
            'installation_date' => 'required',
            'installation_time' => 'required',
            'group_id' => 'required'
        ]);

        $installation = new Installation;
        $installation->fill($request->all());
        $installer->installations->save($installer);

        return $installation;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($installer_id, $id)
    {
        $installer = Installer::findOrFail($installer_id);
        $installation = $installer->installations()->findOrFail($id);
        return $installation;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $installer_id, $id)
    {
        $installer = Installer::findOrFail($installer_id);
        $installation = $installer->installations()->find($id);

        JSONValidator::validate($request, [
            'installation_date' => 'required',
            'installation_time' => 'required',
            'group_id' => 'required'
        ]);

        $installation->fill($request->all());
        $installation->save();

        return $installation;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($installer_id, $id)
    {
        $installer = Installer::findOrFail($installer_id);
        $installation = $installer->installations()->find($id);
        $installation->delete();
        return $installation;
    }
}
