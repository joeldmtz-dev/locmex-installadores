<?php

namespace App\Http\Controllers\Installers;

use App\Http\Controllers\Controller;
use App\Installer;
use App\Library\JSONValidator;
use Illuminate\Http\Request;

class Installers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->input();
        $installers = Installer::with('group')->get();

        if (isset($params['request_date'])) {
            foreach ($installers as $index => $installer){
                $installers[$index]->installations = $installer
                    ->installations_for_date($params['request_date']);
            }

        }

        return $installers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        JSONValidator::validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'second_last_name' => 'required',
            'user_id' => 'required',
            'group_id' => 'required'
        ]);

        $installer = new Installer;
        $installer->fill($request->all());
        $installer->save();

        return $installer;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $installer = Installer::with('group')->findOrFail($id);
        return $installer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $installer = Installer::findOrFail($id);

        JSONValidator::validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'second_last_name' => 'required',
            'user_id' => 'required',
            'group_id' => 'required'
        ]);

        $installer->fill($request->all());
        $installer->save();
        return $installer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $installer = Installer::findOrFail($id);
        $installer->delete();
        return $installer;
    }
}
