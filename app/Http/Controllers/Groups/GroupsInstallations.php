<?php

namespace App\Http\Controllers\Groups;

use App\Group;
use App\Installation;
use App\Library\JSONValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupsInstallations extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $group_id)
    {
        $params = $request->input();
        $group = Group::findOrFail($group_id);
        $installations = $group->installations()->with('group', 'installer', 'units');
        return Installation::filter($installations, $params);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $group_id)
    {
        $group = Group::findOrFail($group_id);

        JSONValidator::validate($request, [
            'address' => 'required',
            'installation_date' => 'required',
            'installation_time' => 'required',
            'installer_id' => 'required'
        ]);

        $installation = new Installation;
        $installation->fill($request->all());
        $group->installations()->save($installation);

        return $installation;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($group_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($id);
        return $installation;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $group_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->instalations()->findOrFail($id);

        JSONValidator::validate($request, [
            'address' => 'required',
            'installation_date' => 'required',
            'installation_time' => 'required',
            'installer_id' => 'required'
        ]);

        $installation->fill($request->all());
        $installation->save();

        return $installation;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($id);
        $installation->delete();
        return $installation;
    }
}
