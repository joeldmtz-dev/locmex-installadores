<?php

namespace App\Http\Controllers\Groups;

use App\Group;
use App\Installer;
use App\Library\JSONValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupsInstallers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($group_id)
    {
        $group = Group::findOrFail($group_id);
        return $group->installers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $group_id)
    {
        $group = Group::findOrFail($group_id);

        JSONValidator::validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'second_last_name' => 'required',
            'user_id' => 'required',
        ]);

        $installer = new Installer;
        $installer->fill($request->all());
        $group->installers()->save($installer);

        return $installer;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($group_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installer = $group->installers()->findOrFail($id);
        return $installer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $group_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installer = $group->installers()->findOrFail($id);

        JSONValidator::validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'second_last_name' => 'required',
            'user_id' => 'required',
        ]);

        $installer->fill($request->all());
        $installer->save();

        return $installer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installer = $group->installers()->findOrFail($id);
        $installer->delete();
        return $installer;
    }
}
