<?php

namespace App\Http\Controllers\Groups;

use App\Group;
use App\Library\JSONValidator;
use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupsInstallationsUnits extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($group_id, $installation_id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($installation_id);
        return $installation->units;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $group_id, $installation_id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($installation_id);

        JSONValidator::validate($request, [
            'imei' => 'required'
        ]);

        $unit = new Unit;
        $unit->fill($request->all());
        $installation->units()->save($unit);

        return $unit;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($group_id, $installation_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($installation_id);
        $unit = $installation->units()->findOrFail($id);
        return $unit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $group_id, $installation_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($installation_id);
        $unit = $installation->units()->findOrFail($id);

        JSONValidator::validate($request, [
            'imei' => 'required'
        ]);

        $unit->fill($request->all());
        $unit->save();

        return $unit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_id, $installation_id, $id)
    {
        $group = Group::findOrFail($group_id);
        $installation = $group->installations()->findOrFail($installation_id);
        $unit = $installation->units()->findOrFail($id);
        $unit->delete();
        return $unit;
    }
}
