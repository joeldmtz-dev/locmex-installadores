<?php

namespace App\Http\Controllers\Installations;

use App\Http\Controllers\Controller;
use App\Installation;
use App\Library\JSONValidator;
use App\Unit;
use Illuminate\Http\Request;

class Installations extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->input();
        $installations = Installation::with('group', 'installer', 'units');
        return Installation::filter($installations, $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        JSONValidator::validate($request, [
            'address' => 'required',
            'installation_date' => 'required',
            'installation_time' => 'required',
            'group_id' => 'required',
            'installer_id' => 'required'
        ]);

        $installation = new Installation;
        $installation->fill($request->all());
        $installation->save();

        $units = $request->unitsList;
        Unit::where('installation_id', $installation->id)
            ->update([
                'installation_id' => null
            ]);

        foreach ($units as $value) {
            $unit = Unit::findOrFail($value);
            $unit->installation_id = $installation->id;
            $unit->save();
        }

        return $installation;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $installation = Installation::with('group', 'installer')->findOrFail($id);
        return $installation;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $installation = Installation::findOrFail($id);

        JSONValidator::validate($request, [
            'installation_date' => 'required',
            'installation_time' => 'required',
            'group_id' => 'required',
            'installer_id' => 'required'
        ]);

        $units = $request->unitsList;
        Unit::where('installation_id', $installation->id)
            ->update([
                'installation_id' => null
            ]);

        foreach ($units as $value) {
            $unit = Unit::findOrFail($value);
            $unit->installation_id = $installation->id;
            $unit->save();
        }

        $installation->fill($request->all());
        $installation->save();

        return $installation;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $installation = Installation::findOrFail($id);
        $installation->delete();
        return $installation;
    }
}
