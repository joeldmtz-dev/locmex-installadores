<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Installer extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'last_name', 'second_last_name', 'user_id', 'group_id'];

    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function group() {
        return $this->belongsTo('App\Group');
    }

    public function installations() {
        return $this->hasMany('App\Installation');
    }

    public function installations_for_date($date) {
        return $this->installations()
            ->with('units')
            ->where('installation_date', $date)->get();
    }
}
