<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'color'];

    protected $dates = ['deleted_at'];

    public function installers() {
        return $this->hasMany('App\Installer');
    }

    public function installations() {
        return $this->hasMany('App\Installation');
    }
}
