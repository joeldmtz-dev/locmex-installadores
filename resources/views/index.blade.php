<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body ng-app="app" ng-cloak flex layout="column">
    <md-toolbar md-scroll-shrink>
        <div class="md-toolbar-tools">
            <img class="logo" src="" alt="LOCMEX" ondragstart="return false;">
            <div flex layout="row" layout-align="end center">

            </div>
        </div>
        <md-progress-linear md-mode="determinate" value="100" class="md-accent"></md-progress-linear>
    </md-toolbar>
    <div layout="column" ng-cloak flex>
        <md-content flex layout="column" layout-padding class="no-padding-gt-xs">
            <div ng-view layout="column" class="no-padding-gt-xs" flex></div>
        </md-content>
    </div>
</body>
<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.4.5/jquery.datetimepicker.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
