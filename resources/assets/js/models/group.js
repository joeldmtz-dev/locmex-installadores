/**
 * Created by joeldmtz on 7/19/17.
 */

angular.module('app').factory('Group', function (restmod) {
    return restmod
        .model('groups')
        .mix({
            installations : { hasMany : 'Installation' },
            installers : { hasMany : 'Installer' }
        })
});