
angular.module('app').factory('Installer', function (restmod) {
    return restmod
        .model('installers')
        .mix({
            installations : { hasMany : 'Installation' }
        })
})