
angular.module('app').factory('Installation', function (restmod) {
    return restmod
        .model('installations')
        .mix({
            units : { hasMany : 'Unit' }
        })
});
