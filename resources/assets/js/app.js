// Dependencies
//require('./bootstrap');
require('angular');
require('angular-route');
require('angular-messages');
require('angular-material');

require('angular-sanitize');
require('angular-material-calendar/dist/angular-material-calendar');

require('angular-restmod');

require('angular-moment');

// App
require('./index');

// Services

// Models
require('./models/group');
require('./models/installations');
require('./models/installers');
require('./models/unit');

// Directives
require('./directives/datetimepicker');


//Controllers
require('./controllers/schedule');