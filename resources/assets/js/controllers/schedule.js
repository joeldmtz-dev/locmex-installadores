if (typeof CryptoJS == 'undefined') {
    var cryptoSrc = '//cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/md5.js';
    var scriptTag = document.createElement('script');
    scriptTag.setAttribute('src', cryptoSrc);
    document.body.appendChild(scriptTag);
}
angular.module('app')
    .controller('ScheduleController', function (
        $scope,
        Group,
        Installation,
        moment,
        MaterialCalendarData,
        $mdDialog
    ) {
        const dayIndicator = function (data) {
            return `<div class="count-indicator">
                        <div>${data.count}</div>
                    </div>`
        };

        $scope.dayEvents = {}
        $scope.events = []
        $scope.currentDate = new Date()

        $scope.load = function (year, month) {
            Installation.$collection({
                byDate : true,
                year,
                month,
            }).$fetch().$then(function (response) {
                for (let dateString in response[0]) {
                    let date = moment(dateString)
                    if (date.isValid()) {
                        MaterialCalendarData.data[dateString] = ""
                        $scope.dayEvents[dateString] = response[0][dateString];
                        MaterialCalendarData.setDayContent(date.toDate(), dayIndicator({
                            count : response[0][dateString].length
                        }))
                    }
                }

                $scope.daySelected($scope.currentDate)
            }, function (error) {

            })
        }

        $scope.load(
            $scope.currentDate.getFullYear(),
            $scope.currentDate.getMonth() + 1
        )

        $scope.formatDate = function (date, format = 'YYYY-MM-DD', parseFormt = 'YYYY-MM-DD') {
            return moment(date, parseFormt).format(format)
        }
        
        $scope.daySelected = function (date) {
            const dateString = moment(date).format('YYYY-MM-DD')
            $scope.currentDate = date

            let events = $scope.dayEvents[dateString] || []
            let groupedEvents = [];

            events.forEach(function (event) {
                let time = moment(event.installation_time, 'HH:mm:ss').toDate()
                time.setMinutes(0)
                let timeString = moment(time).format('HH:mm')

                groupedEvents[timeString] = groupedEvents[timeString] || []
                groupedEvents[timeString].push(event)
            })

            let daylyEvents = []
            for (let time in groupedEvents) {
                daylyEvents.push({ time, events: groupedEvents[time] })
            }

            $scope.events = daylyEvents

        }

        $scope.setDayContent = function (date) {
            const dateString = moment(date).format('YYYY-MM_-DD')
            const events = $scope.dayEvents[dateString] || []

            if (!events.length) return

            return dayIndicator({
                count : events.length
            })
        }
        
        $scope.changeMonth = function (date) {
            $scope.currentDate.setFullYear(date.year)
            $scope.currentDate.setMonth(date.month - 1)
            $scope.currentDate.setDate(1)

            $scope.load(date.year, date.month)
        }

        $scope.textColor = function (color) {
            return idealTextColor(color)
        }

        $scope.newEvent = function (ev) {
            $mdDialog.show({
                controller: 'AddEventController',
                controllerAs: 'ctrl',
                templateUrl: '/views/eventFormView.html',
                targetEvent: ev,
                clickOutsideToClose: false,
                parent: angular.element(document.body),
                fullscreen: true,
                locals: {
                    event : {
                        datetime: $scope.currentDate
                    }
                }
            }).then(function(answer) {
                switch (answer) {
                    case 'reload':
                        $scope.load(
                            $scope.currentDate.getFullYear(),
                            $scope.currentDate.getMonth() + 1
                        )
                        break;
                }
            }, function() {

            });
        }

        $scope.editEvent = function (event, ev) {
            event = angular.copy(event)
            event.datetime = moment(`${event.installation_date} ${event.installation_time}`).format('YYYY-MM-DD HH:mm:ss')

            $mdDialog.show({
                controller: 'AddEventController',
                controllerAs: 'ctrl',
                templateUrl: '/views/eventFormView.html',
                targetEvent: ev,
                clickOutsideToClose: false,
                parent: angular.element(document.body),
                fullscreen: true,
                locals: {
                    event : event
                }
            }).then(function(answer) {
                switch (answer) {
                    case 'reload':
                        $scope.load(
                            $scope.currentDate.getFullYear(),
                            $scope.currentDate.getMonth() + 1
                        )
                        break;
                }
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        }

        function idealTextColor(bgColor) {

            const nThreshold = 105;
            const components = getRGBComponents(bgColor);
            const bgDelta = (components.R * 0.299) + (components.G * 0.587) + (components.B * 0.114);

            return ((255 - bgDelta) < nThreshold) ? "#000000" : "#ffffff";
        }

        function getRGBComponents(color) {

            const r = color.substring(1, 3);
            const g = color.substring(3, 5);
            const b = color.substring(5, 7);

            return {
                R: parseInt(r, 16),
                G: parseInt(g, 16),
                B: parseInt(b, 16)
            };
        }
    })
    .controller('AddEventController', function (
        $scope,
        $mdDialog,
        $q,
        $timeout,
        moment,
        Installer,
        Installation,
        Group,
        Unit,
        event
    ) {
        const self = this;
        let pendingSearch, cancelSearch = angular.noop;
        let lastSearch;

        self.cancel = function() {
            $mdDialog.cancel();
        };

        self.answer = function(answer) {
            $mdDialog.hide(answer);
        };

        self.event = event
        const unit_params = {}

        if (!self.event.units || !self.event.units.length) {
            unit_params.onlyFree = true
        }

        self.loadInstallersData = function () {
            Installer.$collection({
                request_date: moment(self.event.datetime).format('YYYY-MM-DD')
            }).$fetch().$then(function (response) {
                self.installersList = response
                self.allInstallers = loadInstallers();
                self.installers = [];

                if (event.installer_id && self.installers.length === 0) {
                    self.installers.push(self.allInstallers.find(function (installer) {
                        return installer.id === event.installer_id
                    }))
                }

            }, function (error) {

            });
        }
        
        $scope.$watch('ctrl.event.datetime', function (newValue, oldValue) {
            self.loadInstallersData()
        })

        Unit.$collection(unit_params).$fetch().$then(function (response) {
            self.unitsList = response
            self.allUnits = loadUnits()
            self.units = []

            if (event.units) {
                self.units = self.allUnits.filter(function (unit) {
                    return event.units.find(function (event_unit) {
                        return event_unit.id === unit.id
                    })
                })
            }
        }, function (error) {

        })

        Group.$collection().$fetch().$then(function (response) {
            self.groups = response
        }, function (error) {

        })

        self.options = {
            lang: "es",
            timepicker: true,
            format: "Y-m-d H:i:s",
            angularFormat: "yyyy-MM-dd HH:mm:ss",
            dayOfWeekStart: 1
        };

        self.indexSelected = 0;
        self.installersFilterSelected = true;

        self.allUnits = loadUnits();
        self.units = [];
        self.unitsFilterSelected = true;

        self.querySearch = querySearch;
        self.delayedQuerySearch = delayedQuerySearch;

        self.formatDate = function (date, format = 'YYYY-MM-DD', parseFormt = 'YYYY-MM-DD') {
            return moment(date, parseFormt).format(format)
        }

        /**
         * Search for contacts; use a random delay to simulate a remote call
         */
        function querySearch (criteria, dataSet) {
            return criteria ? dataSet.filter(createFilterFor(criteria)) : [];
        }

        /**
         * Async search for contacts
         * Also debounce the queries; since the md-contact-chips does not support this
         */
        function delayedQuerySearch(criteria, dataSet) {
            if ( !pendingSearch || !debounceSearch() )  {
                cancelSearch();

                return pendingSearch = $q(function(resolve, reject) {
                    // Simulate async search... (after debouncing)
                    cancelSearch = reject;
                    $timeout(function() {

                        resolve( self.querySearch(criteria, dataSet) );

                        refreshDebounce();
                    }, Math.random() * 500, true)
                });
            }

            return pendingSearch;
        }

        function refreshDebounce() {
            lastSearch = 0;
            pendingSearch = null;
            cancelSearch = angular.noop;
        }

        /**
         * Debounce if querying faster than 300ms
         */
        function debounceSearch() {
            const now = new Date().getMilliseconds();
            lastSearch = lastSearch || now;

            return ((now - lastSearch) < 300);
        }

        /**
         * Create filter function for a query string
         */
        function createFilterFor(query) {
            const lowercaseQuery = angular.lowercase(query);

            return function filterFn(contact) {
                return (contact._lowername.indexOf(lowercaseQuery) != -1);
            };

        }

        self.filterInstallers = function () {
            self.installers = []
            self.allInstallers = loadInstallers()
        }

        function loadInstallers() {
            if (self.installersList) {
                return self.installersList
                    .map(function (c, index) {
                        const hash = CryptoJS.MD5(c.name);

                        const installer = {
                            id: c.id,
                            name: `${c.name} ${c.last_name} ${c.second_last_name}`,
                            group: c.group.name,
                            group_id: c.group.id,
                            installations: c.installations,
                            image: '//www.gravatar.com/avatar/' + hash + '?s=50&d=retro'
                        };

                        installer._lowername = installer.name.toLowerCase();
                        return installer;
                    })
                    .filter(function (installer) {
                        return installer.group_id === self.event.group_id
                    });
            }
        }

        function loadUnits() {
            if (self.unitsList) {
                return self.unitsList.map(function (c, index) {
                    const unit = {
                        id: c.id,
                        imei: c.imei
                    };
                    unit._lowername = unit.imei.toLowerCase();
                    return unit;
                });
            }
        }

        self.sendEvent = function () {
            self.event.unitsList = self.units.map(function (unit) {
                return unit.id
            })

            let event
            if (!self.event.id) {
                event = Installation.$build(self.event)
            } else {
                event = Installation.$new(self.event.id)
            }

            Object.assign(event, self.event)
            event.installation_date = moment(event.datetime).format('YYYY-MM-DD')
            event.installation_time = moment(event.datetime).format('HH:mm:ss')
            event.installer_id = self.installers[0].id

            event.$save().$then(function (response) {
                self.answer('reload')
            }, function (error) {

            })
        }
    });