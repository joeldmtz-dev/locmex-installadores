/**
 * Created by joeldmtz on 7/19/17.
 */

const module = angular.module('app', [
    'ngRoute',
    'ngMaterial',
    'ngMessages',
    'ngSanitize',
    'materialCalendar',
    'restmod',
    'angularMoment'
]).config(function($routeProvider, restmodProvider){

    $routeProvider.when('/', {
        templateUrl: '/views/schedule.html'
    }).when('/installers', {
        templateUrl: '/views/installers.html'
    }).otherwise({
        redirectTo: '/'
    });

    restmodProvider.rebase({
        $config: {
            style: 'AMSApi',
            urlPrefix: 'api/'
        },
        $hooks: {
            'before-request': function(request) {
                if (angular.isUndefined(request.headers)) {
                    request.headers = {};
                }
                request.headers = angular.extend(request.headers, {
                    'Autorization': `Bearer `
                });
            }
        }
    });

})